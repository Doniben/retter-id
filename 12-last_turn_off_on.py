#!/usr/bin/env python3

from subprocess import call
import platform
import sys
import os

last_shutdown = os.popen('last shutdown')
last_reboot = os.popen('last reboot')
running = os.popen('uptime')

if sys.platform == "linux" or sys.platform == "linux2":
    print("The last shutdown on this linux machine was on: {}".format(last_shutdown.read()))
    print("The last reboot on this linux machine was on: {}".format(last_reboot.read()))
    print("This linux Machine has been running during: {}".format(running.read()))
    
elif sys.platform == "darwin":
    print("The last shutdown on this macOs machine was on: {}".format(last_shutdown.read()))
    print("The last reboot on this macOs machine was on: {}".format(last_reboot.read()))
    print("This macOs Machine has been running during: {}".format(running.read()))
elif sys.platform == "win32":
    call( ["powershell", "-command", "(gcim Win32_OperatingSystem).LastBootUpTime"] )
