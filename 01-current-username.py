#!/usr/bin/env python3
import getpass
from sys import platform

if platform == "win32":
    win_user_name = getpass.getuser()
    print("user microsoft account: {}".format(win_user_name))

elif platform == "darwin":
    mac_user_name = getpass.getuser()
    print("user mac account: {}".format(mac_user_name))

elif platform == "linux" or platform == "linux2":
    lin_user_name = getpass.getuser()
    print("user linux account: {}".format(lin_user_name))
