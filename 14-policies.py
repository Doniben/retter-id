#!/usr/bin/env python3

import platform
import sys
import os

if sys.platform == "linux" or sys.platform == "linux2":
    print("Configuration of security policy in linux see: https://subscription.packtpub.com/book/networking_and_servers/9781785286421/1/ch01lvl1sec10/the-security-policy-of-linux")

elif sys.platform == "darwin":
    print("To understand group policies in macOs go to: https://docs.centrify.com/en/css/2018-html/#page/Managing_Mac_OS_X%2Fadm_understanding_GPs.html%23")

elif sys.platform == "win32":
    print("To see group policies applied use on powershell: rsop.msc, gpresult /Scope User /v, gpresult /Scope Computer /v")
    #Command --> os.system('secedit /export /cfg group-policy.inf /log export.log')
