#!/usr/bin/env python3

import platform
import sys
import os

if sys.platform == "linux" or sys.platform == "linux2":
    last_logs_lin = os.system('lastlog')
    installed_packages = os.system('cat /var/log/dpkg.log')

    print("LINUX LOG ACTIVITY HISTORY")
    print("LAST LOGS:")
    print(last_logs_lin)
    print("INSTALLED PAKAGES")
    print(installed_packages)
    
elif sys.platform == "darwin":
    print("Select between the possibilities in /var/log")

elif sys.platform == "win32":
    print('To get windows event logs see: https://www.blog.pythonlibrary.org/2010/07/27/pywin32-getting-windows-event-logs/')
