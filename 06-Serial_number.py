#!/usr/bin/env python3                                                                                    

import platform

# wmic bios get serialnumber#Windows
# hal-get-property --udi /org/freedesktop/Hal/devices/computer --key system.hardware.uuid#Linux
# ioreg -l | grep IOPlatformSerialNumber#Mac OS X
import os, sys


def getMachine_addr():
    os_type = sys.platform.lower()
    if sys.platform == "win32":
        command = "wmic bios get serialnumber"
    elif sys.platform == "linux" or sys.platform == "linux2":
        command = "hal-get-property --udi /org/freedesktop/Hal/devices/computer --key system.hardware.uuid"
    elif sys.platform == "darwin":
        command = "ioreg -l | grep IOPlatformSerialNumber"
        return os.popen(command).read().replace("\n","").replace("","").replace(" ","")

#output machine serial code: XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXX
serial = str(getMachine_addr())
print("MotherBoard serial Number: " + serial)
