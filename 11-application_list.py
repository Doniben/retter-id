#!/usr/bin/env python3
# Install for windows: pip install winapps


import platform
import sys
import re
import subprocess

PACKAGE_MANAGERS = {
    # "command": "test if package ? exists"-commnad                                                           
    "dpkg": "dpkg -s ?",
    "brew": "brew ls ?"
    # just add new package managers here                                                                      
    }

if sys.platform == "linux" or sys.platform == "linux2":
    # FROM COMMAND LINE:
    #dpkq-query --show
    #dpkg-query --list
    #apt list --installed
    print("WARNING: Works from command line on Linux but it's not still coded on python")
    
    def find_package_manager():
        for pm in PACKAGE_MANAGERS.keys():
            if subprocess.call(["which", pm], stdout=subprocess.PIPE, stderr=subprocess.PIPE) == 0:
                return pm
        raise OSError("Unable to find package manager.")

    def is_package_installed(name):
        return subprocess.call(PACKAGE_MANAGERS[find_package_manager()].replace("?", name)+" > /dev/null 2>&1", shell=True) == 0

    def detect_package_version(name):
        if not is_package_installed(name):
            raise OSError("Unable to find package.")
        try:
            x = subprocess.check_output(name+" --version > /dev/null 2>&1", shell=True)
        except subprocess.CalledProcessError:
            pass
        else:
            a = re.findall("\\d+\\.\\d+\\.\\d+", x)
            if len(a) > 0:
                return a[0]
            b = re.findall("\\d+\\.\\d+", x)
            if len(b) > 0:
                return b[0]

    print(is_package_installed("python"))
    print(detect_package_version("python"))
elif sys.platform == "darwin":
    print("WARNING: Works from command line on macOs but it's not still coded on python")

    def find_package_manager():
        for pm in PACKAGE_MANAGERS.keys():
            if subprocess.call(["which", pm], stdout=subprocess.PIPE, stderr=subprocess.PIPE) == 0:
                return pm
        raise OSError("Unable to find package manager.")

    def is_package_installed(name):
        return subprocess.call(PACKAGE_MANAGERS[find_package_manager()].replace("?", name)+" > /dev/null 2>&1", shell=True) == 0

    def detect_package_version(name):
        if not is_package_installed(name):
            raise OSError("Unable to find package.")
        try:
            x = subprocess.check_output(name+" --version > /dev/null 2>&1", shell=True)
        except subprocess.CalledProcessError:
            pass
        else:
            a = re.findall("\\d+\\.\\d+\\.\\d+", x)
            if len(a) > 0:
                return a[0]
            b = re.findall("\\d+\\.\\d+", x)
            if len(b) > 0:
                return b[0]

    print(is_package_installed("python"))
    print(detect_package_version("python"))
elif sys.platform == "win32":
    #import winapps
    #for app in winapps.list_installed():
    #print(app)
    print("WARNING: Linting applications on windows works but the libreary winapps is not yet installed")
