#!/usr/bin/env python3 
# It is necesary to install sudo apt install samba
# brew install smbutil for mac

import platform
import sys
import os

if sys.platform == "linux" or sys.platform == "linux2":
    print("testing with samba. Needs to take the root password with kwargs")
elif sys.platform == "darwin":
    print("testing with smbutil. Needs to take the root password with kwargs")
elif sys.platform == "win32":
    print("Use net share on cmd")
