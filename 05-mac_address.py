#!/usr/bin/env python3

import platform
import sys
import psutil

if sys.platform == "linux" or sys.platform == "linux2":
    print("MAC Address on Linux: " + psutil.net_if_addrs()['eth0'][1][1])
    #Another OPTION
    """import fcntl
    import socket
    import struct


    def getHwAddr(ifname):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', bytes(ifname, 'utf-8')[:15]))
        return ':'.join('%02x' % b for b in info[18:24])


    def main():
        print(getHwAddr('enp0s8'))


    if __name__ == "__main__":
        main()
        """
elif sys.platform == "darwin":
    print("MAC Address on macOs: " + psutil.net_if_addrs()['en0'][1][1])
    #Another OPTION
    #from uuid import getnode as get_mac
    #mac = get_mac()
    #hex(mac)
    #':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    """ANOTHER ONE
    def getmac(interface):

    try:
    mac = open('/sys/class/net/'+interface+'/address').readline()
    except:
    mac = "00:00:00:00:00:00"

    return mac[0:17]
  myMAC = getmac("wlan0")
  """
elif sys.platform == "win32":
    from getmac import get_mac_address as gma
    print("MAC Address on Windows" + gma())
