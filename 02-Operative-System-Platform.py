#!/usr/bin/env python3


from sys import platform

os_linux = "Operative System: Linux"
os_mac = "Operative System: macOs"
os_windows = "Operative System: Windows"

if platform == "linux" or platform == "linux2":
    print(os_linux)
elif platform == "darwin":
    print(os_mac)
elif platform == "win32":
    print(os_windows)
