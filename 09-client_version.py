#!/usr/bin/env python3                                                                                                                   

import platform
import sys

if sys.platform == "linux" or sys.platform == "linux2":
    print("WARNING: Without information on Linux about client version")
elif sys.platform == "darwin":
    print("WARNING: Without information on macOS about client version")
elif sys.platform == "win32":
    print("WARNING: Without information on windows about client version")
