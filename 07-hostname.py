#!/usr/bin/env python3                                                                                         

import platform
import sys
import socket

if sys.platform == "linux" or sys.platform == "linux2":
    print("Local hostname on Linux: " + socket.gethostname())
    #ANOTHER OPTION
    # h = platform.uname()[1]
elif sys.platform == "darwin":
    print("Local hostname on macOs: " + socket.gethostname())
elif sys.platform == "win32":
    print("Local hostname on Windows: " + socket.gethostname())


