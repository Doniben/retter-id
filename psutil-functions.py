import psutil
print(psutil.virtual_memory())
print('memory % used:', psutil.virtual_memory()[2])
print("+++++++++++++++++++++++++++++++++++++++")
# Number of cores
print(psutil.cpu_count)
print("+++++++++++++++++++++++++++++++++++++++")
# CPU frecuency
print(psutil.cpu_freq())
print("+++++++++++++++++++++++++++++++++++++++")
#Disk Partitions
print(psutil.disk_partitions())
print("+++++++++++++++++++++++++++++++++++++++")
#Disk Usage
print(psutil.disk_usage('/'))
print("+++++++++++++++++++++++++++++++++++++++")
# Net Information
print(psutil.net_if_addrs())
print("+++++++++++++++++++++++++++++++++++++++")
#Sensor Battery
print(psutil.sensors_battery())
print("+++++++++++++++++++++++++++++++++++++++")
#USERS
print(psutil.users())
print("+++++++++++++++++++++++++++++++++++++++")
#Boot Time
print(psutil.boot_time())
print("+++++++++++++++++++++++++++++++++++++++")
