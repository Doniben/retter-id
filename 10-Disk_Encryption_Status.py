#!/usr/bin/env python3                                                                                                                   

import platform
import sys

if sys.platform == "linux" or sys.platform == "linux2":
    print("WARNING: Without information on Linux about Disk Encryption Status")
elif sys.platform == "darwin":
    print("WARNING: Without information on macOS about Disk Encryption Status")
elif sys.platform == "win32":
    print("WARNING: Without information on windows about Disk Encryption Status")
