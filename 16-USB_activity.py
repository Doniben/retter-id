#!/usr/bin/env python3

import platform
import sys
import os

usb_log = os.system("sudo dmesg | grep 'USB'")

if sys.platform == "linux" or sys.platform == "linux2":
    print("LOGS'S USB ACTIVITY ON LINUX:")
    print(usb_log)

elif sys.platform == "darwin":
    print("LOGS'S USB ACTIVITY ON LINUX:")
    print(usb_log)

elif sys.platform == "win32":
    print("Test with this page: https://devblogs.microsoft.com/scripting/use-powershell-to-find-the-history-of-usb-flash-drive-usage/")
