#!/usr/bin/env python3 

import platform
import sys

if sys.platform == "linux" or sys.platform == "linux2":
    print("""
    linux distribution: {}
    distribution version: {}
    """.format(
            platform.linux_distribution()[0],
            platform.linux_distribution()[1]
            ))

elif sys.platform == "darwin":
    print("""
    macOs version: {}
    """.format(
            platform.mac_ver()[0]
            ))
elif sys.platform == "win32":
    print("""
    Windows Os version: {}
    """.format(
        platform.release()
        ))
