#!/usr/bin/env python3

import platform
import sys
import psutil
import subprocess
import re

partitions = ""

if sys.platform == "linux" or sys.platform == "linux2":
    print("MEMORY")
    print("Total Memory: {}".format(psutil.virtual_memory()[0]))
    print("Used Memory: {}".format(psutil.virtual_memory()[3]))
    print("Available Memory: {}".format(psutil.virtual_memory()[1]))
    print("Usage percent: {}".format(psutil.virtual_memory()[2]))
    print("HARDISK")
    partitions = psutil.disk_partitions()
    for i in partitions:
        print("Partition" + i[0])
        print("Partition Type:" + i[2])
    print("Total Disk space: {}".format(psutil.disk_usage('/')[0]))
    print("Total Disk used: {}".format(psutil.disk_usage('/')[1]))
    print("Percentage used: {}".format(psutil.disk_usage('/')[3]))
    print("PROCESSOR")
    command = "cat /proc/cpuinfo"
    print(subprocess.check_output(command, shell=True).strip())
    print("Porcentage cpu usage: {}".format(psutil.cpu_percent()))
    print("Number of threads: {}".format(psutil.cpu_count()))
elif sys.platform == "darwin":
    print("MEMORY")
    print("Total Memory: {}".format(psutil.virtual_memory()[0]))
    print("Used Memory: {}".format(psutil.virtual_memory()[3]))
    print("Available Memory: {}".format(psutil.virtual_memory()[1]))
    print("Usage percent: {}".format(psutil.virtual_memory()[2]))
    print("HARDISK")
    partitions = psutil.disk_partitions()
    for i in partitions:
        print("Partition" + i[0])
        print("Partition Type:" + i[2])
    print("Total Disk space: {}".format(psutil.disk_usage('/')[0]))
    print("Total Disk used: {}".format(psutil.disk_usage('/')[1]))
    print("Percentage used: {}".format(psutil.disk_usage('/')[3]))
    print("PROCESSOR")
    print(subprocess.check_output(['/usr/sbin/sysctl', "-n", "machdep.cpu.brand_string"]).strip())
    print("Porcentage cpu usage: {}".format(psutil.cpu_percent()))
    print("Number of threads: {}".format(psutil.cpu_count()))
elif sys.platform == "win32":
    print("MEMORY")
    print("Total Memory: {}".format(psutil.virtual_memory()[0]))
    print("Used Memory: {}".format(psutil.virtual_memory()[3]))
    print("Available Memory: {}".format(psutil.virtual_memory()[1]))
    print("Usage percent: {}".format(psutil.virtual_memory()[2]))
    print("HARDISK")
    partitions = psutil.disk_partitions()
    for i in partitions:
        print("Partition" + i[0])
        print("Partition Type:" + i[2])
    print("Total Disk space: {}".format(psutil.disk_usage('/')[0]))
    print("Total Disk used: {}".format(psutil.disk_usage('/')[1]))
    print("Percentage used: {}".format(psutil.disk_usage('/')[3]))
    print("PROCESSOR")
    print("Porcentage cpu usage: {}".format(psutil.cpu_percent()))
    print("Number of threads: {}".format(psutil.cpu_count()))
    platform.processor()
    family = platform.processor()
    name = subprocess.check_output(["wmic","cpu","get", "name"]).strip().split("\n")[1]
    print(' '.join([name, family]))

""" OPTIONAL
def get_cpu_type():
    from win32com.client import GetObject
    root_winmgmts = GetObject("winmgmts:root\cimv2")
    cpus = root_winmgmts.ExecQuery("Select * from Win32_Processor")
    return cpus[0].Name
"""





# you can convert that object to a dictionary
# dict(psutil.virtual_memory()._asdict())
