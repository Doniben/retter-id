#!/usr/bin/env python3

import platform
import sys
import socket

hostname = socket.gethostname()
ip_address = socket.gethostbyname(hostname)

if sys.platform == "linux" or sys.platform == "linux2":
    print("local IP Address on linux: " + ip_address)
elif sys.platform == "darwin":
    print("local IP Address on macOs: " + ip_address)
elif sys.platform == "win32":
    print("local IP Address on Windows: " + ip_address)
