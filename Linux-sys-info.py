#!/usr/bin/env python3

import platform
import sys

print("""Python version: {}
linux_distribution: {}
system: {}
machine: {}
platform: {}
uname: {}
version: mac_ver: {}
""".format(
sys.version.split('\n'),
platform.linux_distribution(),
platform.system(),
platform.machine(),
platform.platform(),
platform.uname(),
platform.version(),
platform.mac_ver()[0],
))
